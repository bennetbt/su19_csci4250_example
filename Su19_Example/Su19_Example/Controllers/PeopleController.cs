﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Su19_Example.Controllers
{
    public class PeopleController : Controller
    {

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Bennett()
        {

            return View();
        }

        public IActionResult Arnfield()
        {
            return View();
        }
        public IActionResult Belcher()
        {
            return View();
        }
        public IActionResult Blackman()
        {
            return View();
        }

        public IActionResult Crockett()
        {
            return View();
        }
        public IActionResult Hopkins()
        {
            return View();
        }
        public IActionResult Johnson()
        {
            return View();
        }
        public IActionResult Kelley()
        {
            return View();
        }
        public IActionResult Lawhorn()
        {
            return View();
        }
        public IActionResult Noe()
        {
            return View();
        }
        public IActionResult Sherman()
        {
            return View();
        }

    }
}